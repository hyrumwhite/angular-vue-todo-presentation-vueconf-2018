!window.vueComponents && (window.vueComponents = {});
// Swap out directives
// Using $emit to get data and events over to AngularJS
window.vueComponents.todoList = {
  el:"#todo-list",
  template: `
    <ul id="todo-list">
      <li v-for="(todo, index) of filteredTodos"
          :class="{completed: todo.completed, editing: todo == editedTodo}">
        <div class="view">
          <input class="toggle"
                 type="checkbox"
                 v-model="todo.completed"
                 @input="$emit('toggleCompleted', todo)">
          <label @click.right="$emit('editTodo', todo)">{{todo.title}}</label>
          <button class="destroy" @click="$emit('removeTodo', todo)"></button>
        </div>
        <form @submit="$emit('saveEdits', todo)">
          <input ref="editor"
                 class="edit"
                 v-model="todo.title"
                 @keyup.esc="$emit('revertEdits', todo)"
                 @blur="$emit('saveEdits', todo, 'blur')">
        </form>
      </li>
    </ul>
  `,
}
